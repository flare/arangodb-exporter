package main

import (
	"encoding/json"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/solher/arangolite"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

const namespace = "arangodb"

var (
	arangoScheme   = env("ARANGODB_SCHEME", "https")
	arangoHost     = env("ARANGODB_HOST", "localhost")
	arangoPort     = env("ARANGODB_PORT", "8529")
	arangoUser     = env("ARANGODB_USER")
	arangoPassword = env("ARANGODB_PASSWORD")
)

var db *arangolite.DB

func env(key string, defaultValue ...string) string {
	val := os.Getenv(key)
	if val == "" && len(defaultValue) > 0 {
		return defaultValue[0]
	}

	return val
}

func main() {
	url := fmt.Sprintf("%s://%s:%s", arangoScheme, arangoHost, arangoPort)

	db = arangolite.New().
		LoggerOptions(true, true, true).
		Connect(url, "_system", arangoUser, arangoPassword)

	exporter := &Exporter{}
	prometheus.MustRegister(exporter)

	http.Handle("/metrics", prometheus.Handler())
	log.Fatal(http.ListenAndServe(":9241", nil))
}

func getStatistics() (*GetStatisticsResult, error) {
	bytes, err := db.Run(&GetStatistics{})
	if err != nil {
		return nil, err
	}

	var res GetStatisticsResult
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func getStatisticsDescription() (*GetStatisticsDescriptionResult, error) {
	bytes, err := db.Run(&GetStatisticsDescription{})
	if err != nil {
		return nil, err
	}

	var res GetStatisticsDescriptionResult
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func getVersion() (*GetVersionResult, error) {
	bytes, err := db.Run(&GetVersion{})
	if err != nil {
		return nil, err
	}

	var res GetVersionResult
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

type Exporter struct {
	version     string
	description *GetStatisticsDescriptionResult
	figureMap   map[string]map[string]StatisticsFigure
	descMap     map[string]map[string]*prometheus.Desc
}

func (e *Exporter) loadDescriptions() error {
	version, err := getVersion()
	if err != nil {
		return err
	}

	if e.version == version.Version {
		return nil
	}

	e.version = version.Version

	desc, err := getStatisticsDescription()
	if err != nil {
		return err
	}

	e.description = desc

	e.figureMap = make(map[string]map[string]StatisticsFigure)
	e.descMap = make(map[string]map[string]*prometheus.Desc)
	for _, group := range desc.Groups {
		e.figureMap[group.Group] = make(map[string]StatisticsFigure)
		e.descMap[group.Group] = make(map[string]*prometheus.Desc)
	}

	for _, figure := range desc.Figures {
		e.figureMap[figure.Group][figure.Identifier] = figure

		fqn := prometheus.BuildFQName(namespace, figure.Group, getName(&figure))
		help := figure.Description

		e.descMap[figure.Group][figure.Identifier] = prometheus.NewDesc(fqn, help, nil, nil)
	}

	return nil
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	err := e.loadDescriptions()
	if err != nil {
		ch <- prometheus.NewInvalidDesc(err)
		return
	}

	for _, figure := range e.description.Figures {
		desc := e.descMap[figure.Group][figure.Identifier]
		ch <- desc
	}
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	err := e.loadDescriptions()
	if err != nil {
		log.Println(err)
		return
	}

	stats, err := getStatistics()
	if err != nil {
		log.Println(err)
		return
	}

	for group, figureMap := range stats.Statistics {
		for identifier, value := range figureMap {
			fig := e.figureMap[group][identifier]
			desc := e.descMap[group][identifier]

			switch fig.Type {
			case StatisticsFigureTypeAccumulated:
				metric := prometheus.MustNewConstMetric(
					desc,
					prometheus.CounterValue,
					value.(float64),
				)
				ch <- metric
			case StatisticsFigureTypeCurrent:
				metric := prometheus.MustNewConstMetric(
					desc,
					prometheus.GaugeValue,
					value.(float64),
				)
				ch <- metric
			case StatisticsFigureTypeDistribution:
				distribution := value.(DistributionStatistic)

				buckets := make(map[float64]uint64)
				sum := 0
				for i, bucket := range fig.Cuts {
					sum += distribution.Counts[i]
					buckets[bucket] = uint64(sum)
				}

				metric := prometheus.MustNewConstHistogram(
					desc,
					uint64(distribution.Count),
					distribution.Sum,
					buckets,
				)

				ch <- metric
			}
		}
	}
}

func getName(figure *StatisticsFigure) string {
	name := regexp.MustCompile("[A-Z]").ReplaceAllStringFunc(figure.Identifier, func(match string) string {
		return "_" + strings.ToLower(match)
	})

	return name + "_" + figure.Units
}
